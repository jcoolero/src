VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmClient 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Client"
   ClientHeight    =   9405
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   12000
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9405
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   Begin MSDataGridLib.DataGrid dgClient 
      Height          =   7725
      Left            =   150
      TabIndex        =   9
      Top             =   1530
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   13626
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   -1  'True
      Appearance      =   0
      HeadLines       =   1
      RowHeight       =   15
      TabAction       =   1
      RowDividerStyle =   4
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   4
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdEdit 
      Caption         =   "&Edit"
      Height          =   465
      Left            =   1350
      TabIndex        =   8
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Delete"
      Height          =   465
      Left            =   2580
      TabIndex        =   7
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "&Add"
      Height          =   465
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Height          =   465
      Left            =   3810
      TabIndex        =   5
      Top             =   960
      Width           =   1305
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "&Search"
      Height          =   465
      Left            =   3000
      TabIndex        =   4
      Top             =   420
      Width           =   1215
   End
   Begin VB.TextBox txtFirstName 
      Height          =   345
      Left            =   1230
      TabIndex        =   1
      Top             =   510
      Width           =   1695
   End
   Begin VB.TextBox txtLastName 
      Height          =   345
      Left            =   1230
      TabIndex        =   0
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "First Name"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   120
      TabIndex        =   3
      Top             =   540
      Width           =   1065
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Last Name"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   150
      Width           =   1065
   End
End
Attribute VB_Name = "frmClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rs As New ADODB.Recordset
Dim obj As PageSet.PrinterControl

Private Sub cmdAdd_Click()
frmClientEntry.clientId = ""
frmClientEntry.Show 1
rs.Requery
End Sub

Private Sub cmdDelete_Click()
If Not rs.EOF Then
    If MsgBox("Deleting Record with id " + rs.Fields("id") + "?", vbYesNo) Then
        If dao.deleteClient(rs.Fields("id")) Then
            MsgBox "Successfully deleted."
            rs.Requery
        End If
    End If
End If
End Sub

Private Sub cmdEdit_Click()
If Not rs.EOF Then
    frmClientEntry.clientId = rs.Fields("id")
    frmClientEntry.Show 1
    rs.Requery
End If
End Sub

Private Sub cmdPrint_Click()
If Not rs.EOF Then
    Set obj = New PrinterControl
    obj.ChngOrientationLandscape
    Set drReport.DataSource = dbSRC.Execute("select count(*) from client")
    Dim fullName As String
    fullName = rs.Fields("lastName") + ", " + rs.Fields("firstName") + " " + Left(rs.Fields("middleName"), 1) + "."
    drReport.Sections(1).Controls("lblName").Caption = fullName
    drReport.Sections(1).Controls("lblImgClientName").Caption = fullName
    Set drReport.Sections(1).Controls("imgClient").Picture = LoadPicture(dao.getPicPath + "\" + rs.Fields("id"))
    drReport.Sections(1).Controls("lblReferenceNo").Caption = rs.Fields("referenceNo")
    drReport.Sections(1).Controls("lblOfficerNumber").Caption = dao.getConfig.Fields("OfficerNumber")
    drReport.Sections(1).Controls("lblOfficerName").Caption = dao.getConfig.Fields("OfficerName")
    drReport.Sections(1).Controls("lblPresidentName").Caption = dao.getConfig.Fields("PresidentName")
    drReport.Sections(1).Controls("lblReleaseDate").Caption = Format(rs.Fields("releaseDate"), "MMMM dd, YYYY")
    drReport.Show 1
End If
End Sub

Private Sub cmdSearch_Click()
Set rs = dao.searchClients(txtFirstName, txtLastName)
Set dgClient.DataSource = rs
End Sub


Private Sub Form_Load()
Set rs = dao.getClients
Set dgClient.DataSource = rs
End Sub

Private Sub txtFirstName_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, cmdSearch
End Sub

Private Sub txtLastName_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, txtFirstName
End Sub
