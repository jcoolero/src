VERSION 5.00
Begin VB.MDIForm frmMain 
   BackColor       =   &H8000000C&
   Caption         =   "SRC"
   ClientHeight    =   3120
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   4680
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuClient 
         Caption         =   "&Client"
      End
      Begin VB.Menu cmdConfig 
         Caption         =   "C&onfig"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuLogout 
         Caption         =   "&Logout"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdConfig_Click()
frmConfig.Show 1
End Sub

Private Sub MDIForm_Activate()
If username = "" Then
    frmLogin.Show 1
End If
End Sub

Private Sub mnuClient_Click()
frmClient.Show
End Sub

Private Sub mnuLogout_Click()
End
End Sub
