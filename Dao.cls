VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Dim adoCommand As ADODB.Command
Dim rs As ADODB.Recordset

Private Sub Class_Initialize()
Set adoCommand = New ADODB.Command
Set rs = New ADODB.Recordset
adoCommand.ActiveConnection = dbSRC
End Sub

Public Function validUser(username As String, password As String) As Boolean
adoCommand.CommandText = "select count(*) as result from user where username=? and password=?"
addParameter adVarChar, username
addParameter adVarChar, password
Set rs = adoCommand.Execute
validUser = rs.Fields("result") > 0
End Function

Private Function addParameter(dataType As DataTypeEnum, value As Variant)
adoCommand.Parameters.Append adoCommand.CreateParameter("", dataType, adParamInput, 255, value)
End Function

