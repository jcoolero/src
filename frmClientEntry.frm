VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmClientEntry 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "ClientEntry"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   3540
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   3540
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCamera 
      Caption         =   "&Camera"
      Height          =   285
      Left            =   2160
      TabIndex        =   12
      Top             =   480
      Width           =   1095
   End
   Begin MSComCtl2.DTPicker dtpReleaseDate 
      Height          =   315
      Left            =   1770
      TabIndex        =   4
      Top             =   3600
      Width           =   1725
      _ExtentX        =   3043
      _ExtentY        =   556
      _Version        =   393216
      Format          =   27459585
      CurrentDate     =   41924
   End
   Begin VB.TextBox txtReferenceNo 
      Height          =   345
      Left            =   1770
      TabIndex        =   3
      Top             =   3210
      Width           =   1695
   End
   Begin MSComDlg.CommonDialog cdPic 
      Left            =   2370
      Top             =   1230
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdAttached 
      Caption         =   "&Attach"
      Height          =   285
      Left            =   2160
      TabIndex        =   9
      Top             =   90
      Width           =   1095
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Height          =   495
      Left            =   2280
      TabIndex        =   5
      Top             =   4050
      Width           =   1185
   End
   Begin VB.TextBox txtMiddleName 
      Height          =   345
      Left            =   1770
      TabIndex        =   2
      Top             =   2820
      Width           =   1695
   End
   Begin VB.TextBox txtLastName 
      Height          =   345
      Left            =   1770
      TabIndex        =   0
      Top             =   2040
      Width           =   1695
   End
   Begin VB.TextBox txtFirstName 
      Height          =   345
      Left            =   1770
      TabIndex        =   1
      Top             =   2430
      Width           =   1695
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Reference No"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   60
      TabIndex        =   11
      Top             =   3210
      Width           =   1665
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Release Date"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   60
      TabIndex        =   10
      Top             =   3600
      Width           =   1665
   End
   Begin VB.Image imgPic 
      BorderStyle     =   1  'Fixed Single
      Height          =   1965
      Left            =   60
      Stretch         =   -1  'True
      Top             =   30
      Width           =   1935
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Middle Name"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   60
      TabIndex        =   8
      Top             =   2820
      Width           =   1695
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Last Name"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   60
      TabIndex        =   7
      Top             =   2040
      Width           =   1695
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "First Name"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   60
      TabIndex        =   6
      Top             =   2430
      Width           =   1695
   End
End
Attribute VB_Name = "frmClientEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public picFileName As String
Public clientId As String
Dim rs As ADODB.Recordset
Private Sub cmdAttached_Click()
On Error Resume Next
cdPic.CancelError = True
cdPic.Flags = cdlOFNHideReadOnly + cdlOFNPathMustExist + cdlOFNFileMustExist
cdPic.Filter = "JPG (*.jpg)|*.jpg|Gif (*.gif)|*.gif"
cdPic.fileName = ""
cdPic.ShowOpen
imgPic.Picture = LoadPicture()
picFileName = ""
imgPic.Picture = LoadPicture(cdPic.fileName)
picFileName = cdPic.fileName
End Sub

Private Sub cmdSave_Click()
Dim id As String
If clientId <> "" Then
    If dao.updateClient(clientId, txtFirstName, txtLastName, txtMiddleName, txtReferenceNo, dtpReleaseDate.value) Then
        copyFile picFileName, clientId
        MsgBox "Successfully updated."
        Unload Me
    End If
Else
    id = generateId
    If dao.addClient(id, txtFirstName, txtLastName, txtMiddleName, txtReferenceNo, dtpReleaseDate.value) Then
        copyFile picFileName, id
        MsgBox "Successfully added."
        Unload Me
    End If
End If
End Sub

Function copyFile(fileName As String, id As String)
Dim Destination As String
Destination = dao.getPicPath + "\" + id
If (fileName <> Destination) And fileName <> "" Then
    If isFileExist(Destination) Then
        Kill Destination
    End If
    FileCopy fileName, Destination
End If
End Function

Private Sub cmdCamera_Click()
On Error Resume Next
frmCamera.Show 1
imgPic.Picture = LoadPicture()
imgPic.Picture = LoadPicture(picFileName)
End Sub

Private Sub Form_Load()
If clientId <> "" Then
    Set rs = dao.getClient(clientId)
    txtFirstName = rs.Fields("firstName")
    txtLastName = rs.Fields("lastName")
    txtMiddleName = rs.Fields("middleName")
    dtpReleaseDate.value = rs.Fields("releaseDate")
    txtReferenceNo = rs.Fields("referenceNo")
    picFileName = dao.getPicPath + "\" + clientId
    If isFileExist(picFileName) Then
        imgPic.Picture = LoadPicture(picFileName)
    Else
        imgPic.Picture = LoadPicture()
    End If
Else
    dtpReleaseDate.value = Now
    picFileName = ""
End If
End Sub

Private Function isFileExist(fileName As String) As Boolean
    isFileExist = Dir(fileName, vbNormal) <> ""
End Function

Private Sub txtFirstName_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, txtMiddleName
End Sub

Private Sub txtLastName_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, txtFirstName
End Sub

Private Sub txtMiddleName_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, txtReferenceNo
End Sub
Private Sub txtReferenceNo_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, dtpReleaseDate
End Sub
Private Sub dtpReleaseDate_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, cmdSave
End Sub
