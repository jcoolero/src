VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DaoService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Dim adoCommand As ADODB.Command

Private Function setCommand()
Set adoCommand = New ADODB.Command
adoCommand.ActiveConnection = dbSRC
End Function

Public Function validUser(username As String, password As String) As Boolean
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "select count(*) as result from user where username=? and password=?"
addParameter adVarChar, username
addParameter adVarChar, password
Set rs = adoCommand.Execute
validUser = rs.Fields("result") > 0
End Function
Public Function addClient(id As String, firstName As String, lastName As String, middleName As String, referenceNo As String, releaseDate As Date) As Boolean
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "insert into client(id,firstName,lastName,middleName,referenceNo,releaseDate) values(?,?,?,?,?,?)"
addParameter adVarChar, id
addParameter adVarChar, firstName
addParameter adVarChar, lastName
addParameter adVarChar, middleName
addParameter adVarChar, referenceNo
addParameter adDate, releaseDate
adoCommand.Execute
addClient = True
End Function
Public Function updateClient(id As String, firstName As String, lastName As String, middleName As String, referenceNo As String, releaseDate As Date) As Boolean
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "update client set firstName=?,lastName=?,middleName=?,referenceNo=?,releaseDate=? where id = ?"
addParameter adVarChar, firstName
addParameter adVarChar, lastName
addParameter adVarChar, middleName
addParameter adVarChar, referenceNo
addParameter adDate, releaseDate
addParameter adVarChar, id
adoCommand.Execute
updateClient = True
End Function
Public Function deleteClient(id As String) As Boolean
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "delete from client where id = ?"
addParameter adVarChar, id
adoCommand.Execute
deleteClient = True
End Function
Public Function getClients() As Recordset
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "select * from client"
Set getClients = adoCommand.Execute
End Function
Public Function getClient(id As String) As Recordset
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "select * from client where id = ?"
addParameter adVarChar, id
Set getClient = adoCommand.Execute
End Function
Public Function getConfig() As Recordset
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "select presidentName,officerName,officerNumber,picPath from config"
Set getConfig = adoCommand.Execute
End Function

Public Function updateConfig(presidentName As String, officerName As String, officerNumber As String, picPath As String) As Boolean
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "update config set presidentName=?,officerName=?,officerNumber=?,picPath=?"
addParameter adVarChar, presidentName
addParameter adVarChar, officerName
addParameter adVarChar, officerNumber
addParameter adVarChar, picPath
adoCommand.Execute
updateConfig = True
End Function
Public Function searchClients(firstName As String, lastName As String) As Recordset
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "select * from client where firstName like ? and lastName like ?"
addParameter adVarChar, firstName + "%"
addParameter adVarChar, lastName + "%"
Set searchClients = adoCommand.Execute
End Function
Private Function addParameter(dataType As DataTypeEnum, value As Variant)
adoCommand.Parameters.Append adoCommand.CreateParameter("", dataType, adParamInput, 255, value)
End Function
Public Function getPicPath() As String
Dim rs As ADODB.Recordset
setCommand
adoCommand.CommandText = "select picPath from config"
getPicPath = adoCommand.Execute.Fields("picPath")
End Function
