VERSION 5.00
Begin VB.Form frmCamera 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Camera"
   ClientHeight    =   5340
   ClientLeft      =   45
   ClientTop       =   645
   ClientWidth     =   9990
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCamera.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   5340
   ScaleWidth      =   9990
   StartUpPosition =   2  'CenterScreen
   Begin SRC.DSPreview dspPreview 
      Height          =   3975
      Left            =   0
      Top             =   360
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   7011
   End
   Begin VB.PictureBox picSnapshot 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      ForeColor       =   &H80000008&
      Height          =   3960
      Left            =   5040
      ScaleHeight     =   3930
      ScaleWidth      =   4800
      TabIndex        =   3
      Top             =   360
      Width           =   4830
   End
   Begin VB.CommandButton cmdSnap 
      Caption         =   "Snap"
      Enabled         =   0   'False
      Height          =   495
      Left            =   4380
      TabIndex        =   2
      Top             =   4740
      Width           =   1215
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Snapshot"
      Height          =   255
      Left            =   5040
      TabIndex        =   1
      Top             =   60
      Width           =   4830
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Preview"
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   4830
   End
   Begin VB.Menu mnuCameras 
      Caption         =   "Cameras"
      Begin VB.Menu mnuCamerasChoice 
         Caption         =   "none"
         Enabled         =   0   'False
         Index           =   0
      End
      Begin VB.Menu mnuCamerasDiv1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCamerasAddNew 
         Caption         =   "Add new camera..."
      End
      Begin VB.Menu mnuCamerasDiv2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCamerasRemove 
         Caption         =   "Remove camera list"
         Enabled         =   0   'False
      End
   End
End
Attribute VB_Name = "frmCamera"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private SelectedCamera As Integer '-1 means none selected.

Private Sub DeselectFailedCamera(ByVal Error As Long)
    Dim CameraName As String
    
    With mnuCamerasChoice(SelectedCamera)
        .Checked = False
        CameraName = .Caption
    End With
    SelectedCamera = -1
    SaveSettings
    MsgBox "Selected camera failed, may not be connected:" & vbNewLine _
         & vbNewLine _
         & CameraName & vbNewLine _
         & vbNewLine _
         & "BuildGraph error " & CStr(Error), _
           vbOKOnly Or vbInformation
End Sub

Private Function IsCameraInMenu(ByVal CameraName As String) As Boolean
    Dim c As Integer
    
    If SelectedCamera >= 0 Then
        For c = 0 To mnuCamerasChoice.UBound
            If CameraName = mnuCamerasChoice(c).Caption Then
                IsCameraInMenu = True
                Exit For
            End If
        Next
    End If
End Function

Private Sub LoadSettings()
    Dim F As Integer
    Dim c As Integer
    Dim CameraName As String
    
    SelectedCamera = -1 'None.
    On Error Resume Next
    GetAttr "Settings.txt"
    If Err.Number = 0 Then
        On Error GoTo 0
        F = FreeFile(0)
        Open "Settings.txt" For Input As #F
        Input #F, SelectedCamera
        Do Until EOF(F)
            Input #F, CameraName
            If c > 0 Then Load mnuCamerasChoice(c)
            With mnuCamerasChoice(c)
                .Enabled = True
                .Caption = CameraName
                .Checked = c = SelectedCamera
            End With
            c = c + 1
        Loop
        Close #F
        mnuCamerasRemove.Enabled = True
    End If
End Sub

Private Sub SaveSettings()
    Dim F As Integer
    Dim c As Integer
    
    F = FreeFile(0)
    Open "Settings.txt" For Output As #F
    Write #F, SelectedCamera
    For c = 0 To mnuCamerasChoice.UBound
        Write #F, mnuCamerasChoice(c).Caption
    Next
    Close #F
End Sub

Private Sub cmdSnap_Click()
    Dim Pic As StdPicture
    
    MousePointer = vbHourglass
    cmdSnap.Enabled = False
    Set Pic = dspPreview.Snap()
    If Pic Is Nothing Then
        MsgBox "Snap failed." & vbNewLine _
             & vbNewLine _
             & "Error " & CStr(dspPreview.Error)
    Else
        With picSnapshot
            .PaintPicture Pic, 0, 0, .ScaleWidth, .ScaleHeight
        End With
    End If
    cmdSnap.Enabled = True
    MousePointer = vbDefault
    frmClientEntry.picFileName = App.Path + "\" + generateId + ".bmp"
    SavePicture picSnapshot.Image, frmClientEntry.picFileName
End Sub

Private Sub Form_Load()
    Dim StartResult As Integer
    
    LoadSettings
    If SelectedCamera >= 0 Then
        If dspPreview.StartCamera(mnuCamerasChoice(SelectedCamera).Caption) Then
            cmdSnap.Enabled = True
        Else
            DeselectFailedCamera dspPreview.Error
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'Make sure we break the link, don't want to have a memory
    'leak or hang the camera or something:
    dspPreview.StopCamera
    
    Unload frmCameraConfig
End Sub

Private Sub mnuCamerasAddNew_Click()
    frmCameraConfig.Show vbModal, Me
    If frmCameraConfig.Oked Then
        If IsCameraInMenu(frmCameraConfig.CameraName) Then
            MsgBox "Camera:" & vbNewLine _
                 & vbNewLine _
                 & frmCameraConfig.CameraName & vbNewLine _
                 & vbNewLine _
                 & "Is already in the menu."
        Else
            dspPreview.StopCamera
            cmdSnap.Enabled = False
            If dspPreview.StartCamera(frmCameraConfig.CameraName) Then
                cmdSnap.Enabled = True
                If SelectedCamera >= 0 Then
                    mnuCamerasChoice(SelectedCamera).Checked = False
                End If
                If SelectedCamera >= 0 Then
                    SelectedCamera = mnuCamerasChoice.UBound + 1
                    Load mnuCamerasChoice(SelectedCamera)
                Else
                    SelectedCamera = 0
                End If
                With mnuCamerasChoice(SelectedCamera)
                    .Caption = frmCameraConfig.CameraName
                    .Checked = True
                    .Enabled = True
                End With
                SaveSettings
                mnuCamerasRemove.Enabled = True
            Else
                MsgBox "This doesn't seems to be a valid webcam:" & vbNewLine _
                     & vbNewLine _
                     & frmCameraConfig.CameraName & vbNewLine _
                     & vbNewLine _
                     & "BuildGraph error " & CStr(Error), _
                       vbOKOnly Or vbInformation
                'Try to go back to previous camera.
                If SelectedCamera > -1 Then
                    If dspPreview.StartCamera(mnuCamerasChoice(SelectedCamera).Caption) Then
                        cmdSnap.Enabled = True
                    Else
                        DeselectFailedCamera dspPreview.Error
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub mnuCamerasChoice_Click(Index As Integer)
    If Index <> SelectedCamera Then
        If SelectedCamera >= 0 Then
            dspPreview.StopCamera
            mnuCamerasChoice(SelectedCamera).Checked = False
        End If
        SelectedCamera = Index
        mnuCamerasChoice(SelectedCamera).Checked = True
        If dspPreview.StartCamera(mnuCamerasChoice(SelectedCamera).Caption) Then
            cmdSnap.Enabled = True
            SaveSettings
        Else
            DeselectFailedCamera dspPreview.Error
        End If
    End If
End Sub

Private Sub mnuCamerasRemove_Click()
    Dim c As Integer
    
    dspPreview.StopCamera
    cmdSnap.Enabled = False
    SelectedCamera = -1
    With mnuCamerasChoice(0)
        .Caption = "none"
        .Enabled = False
    End With
    For c = mnuCamerasChoice.UBound To 1 Step -1
        Unload mnuCamerasChoice(c)
    Next
    mnuCamerasRemove.Enabled = False
    On Error Resume Next
    Kill "Settings.txt"
End Sub
