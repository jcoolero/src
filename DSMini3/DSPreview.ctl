VERSION 5.00
Begin VB.UserControl DSPreview 
   Appearance      =   0  'Flat
   CanGetFocus     =   0   'False
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ClipBehavior    =   0  'None
   ClipControls    =   0   'False
   HitBehavior     =   0  'None
   PaletteMode     =   4  'None
   ScaleHeight     =   3600
   ScaleWidth      =   4800
   ToolboxBitmap   =   "DSPreview.ctx":0000
   Begin VB.Shape shpBorder 
      BorderColor     =   &H80000006&
      Height          =   3375
      Left            =   120
      Top             =   120
      Width           =   4515
   End
End
Attribute VB_Name = "DSPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
'DSPreview control
'
'Requires a reference to:
'
'   ActiveMovie control type library (quartz.dll).
'

'=== Public Enums ====================================================

Public Enum BorderStyles
    bsNone
    bsFixedSingle
End Enum
#If False Then
Dim bsNone, bsFixedSingle
#End If

'FILTER_STATE values, should have been defined in Quartz.dll,
'but another item Microsoft left out.
Public Enum FILTER_STATE
    State_NotStarted = -1 'No FilgraphManager object, i.e. preview not started.
    State_Stopped = 0
    State_Paused = 1
    State_Running = 2
End Enum
#If False Then
Dim State_Error, State_Stopped, State_Paused, State_Running
#End If

'=== Private Declarations ============================================

Private Const WS_BORDER = &H800000
Private Const WS_DLGFRAME = &H400000
Private Const WS_SYSMENU = &H80000
Private Const WS_THICKFRAME = &H40000
Private Const MASKBORDERLESS = Not (WS_BORDER Or WS_DLGFRAME Or WS_SYSMENU Or WS_THICKFRAME)
Private Const MASKBORDERMIN = Not (WS_DLGFRAME Or WS_SYSMENU Or WS_THICKFRAME)

Private Const E_FAIL As Long = &H80004005

'These are "scripts" followed by BuildGraph() below to create a
'DirectShow FilterGraph for webcam viewing.
'
'FILTERLIST is incomplete, and must be prepended with the name
'of your webcam's Video Capture Source filter.  Since there may
'be multiples, FILTERLIST begins with "~Capture" which is used
'when BuildGraph() interprets this script to select one having
'a pin named "Capture".
Private Const FILTERLIST As String = _
        "~Capture|" _
      & "AVI Decompressor|" _
      & "Color Space Converter|" _
      & "Video Renderer"
Private Const CONNECTIONLIST As String = _
        "Capture~XForm In|" _
      & "XForm Out~Input|" _
      & "XForm Out~VMR Input0"

Private fgmVidCap As QuartzTypeLib.FilgraphManager 'Not "Is Nothing" means camera is previewing.
Private bv2VidCap As QuartzTypeLib.IBasicVideo2
Private vwVidCap As QuartzTypeLib.IVideoWindow
Private SelectedCamera As Integer '-1 means none selected.
Private InsideWidth As Double 'UserControl's ScaleMode units.
Private BorderOffset As Long 'Pixels.
Private DesignHeight As Single 'UserControl's initial height.

Private AspectRatio As Double

'=== Private Property Caches =========================================

Private mBorderStyle As BorderStyles

'=== Public Properties ===============================================

'Error values:
'
'           0: No error.
'       1 - 9: Failure processing FILTERLIST script at item "Error."
'   101 - 109: Failure processing CONNECTIONLIST script at item "Error."
'         200: Not running preview.
'         300: Failed to enter paused state.
'         400: Failed to resume into running state.
'         500: StartCamera() called but already started.
'         600: Snap() failed to create StdPicture snapshot.
Public Error As Integer

Public Property Get BackColor() As OLE_COLOR
    BackColor = UserControl.BackColor
End Property

Public Property Let BackColor(ByVal RHS As OLE_COLOR)
    UserControl.BackColor = RHS
    PropertyChanged "BackColor"
End Property

Public Property Get BorderStyle() As BorderStyles
    BorderStyle = mBorderStyle
End Property

Public Property Let BorderStyle(ByVal RHS As BorderStyles)
    If bsNone > RHS Or RHS > bsFixedSingle Then Err.Raise 5, TypeName(Me)
    mBorderStyle = RHS
    shpBorder.Visible = mBorderStyle = bsFixedSingle
    PropertyChanged "BorderStyle"
End Property

Public Property Get State() As FILTER_STATE
    If fgmVidCap Is Nothing Then
        State = State_NotStarted
    Else
        fgmVidCap.GetState 0, State
    End If
End Property

'=== Public Methods ==================================================

Public Function PauseCamera() As Boolean
    'Returns True on success, error in Error.
    Const PauseWaitMs As Long = 16
    Dim State As FILTER_STATE
    
    If fgmVidCap Is Nothing Then
        Error = 200
        Exit Function
    End If
    
    With fgmVidCap
        .Pause
        Do
            .GetState PauseWaitMs, State
        Loop Until State = State_Paused Or Err.Number = E_FAIL
        If Err.Number = E_FAIL Then
            Error = 300
            Exit Function
        End If
    End With
    
    PauseCamera = True
End Function

Public Function ResumeCamera() As Boolean
    'Returns True on success, error in Error.
    Const ResumeWaitMs As Long = 16
    Dim State As FILTER_STATE
    
    If fgmVidCap Is Nothing Then
        Error = 200
        Exit Function
    End If
    
    With fgmVidCap
        .Run
        Do
            .GetState ResumeWaitMs, State
            Error = Err.Number
        Loop Until State = State_Running Or Error = E_FAIL
        If Error = E_FAIL Then
            Error = 400
            Exit Function
        Else
        Error = 0
        End If
    End With
    
    ResumeCamera = True
End Function

Public Function Snap() As StdPicture
    'Returns Nothing on failure.
    Const PauseWaitMs As Long = 16
    Const biSize = 40 'BITMAPINFOHEADER and not BITMAPV4HEADER, etc. but we don't get those.
    Dim State As FILTER_STATE
    Dim Size As Long
    Dim DIB() As Long
    
    If fgmVidCap Is Nothing Then
        Error = 200
        Exit Function
    End If
    
    With fgmVidCap
        .Pause
        Do
            .GetState PauseWaitMs, State
            Error = Err.Number
        Loop Until State = State_Paused Or Error = E_FAIL
        If Error = E_FAIL Then
            Error = 300
            Exit Function
        Else
            Error = 0
        End If
    
        With bv2VidCap
            'Estimate size.  Correct for 32-bit RGB and generous
            'for anything with fewer bits per pixel, compressed,
            'or palette-ized (we hope).
            Size = biSize + .VideoWidth * .VideoHeight
            ReDim DIB(Size - 1)
            Size = Size * 4 'To bytes.
            .GetCurrentImage Size, DIB(0)
        End With
        
        .Run
    End With
    
    Set Snap = LongDIB2Picture(DIB)
    If Snap Is Nothing Then
        Error = 600
    End If
End Function

Public Function StartCamera(ByVal CameraName As String) As Boolean
    'Returns True on success, error in Error.
    
    If Not fgmVidCap Is Nothing Then
        Error = 500
        Exit Function
    End If
    
    Set fgmVidCap = New QuartzTypeLib.FilgraphManager
    'Tack camera name onto FILTERLIST and try to start it.  Add 1 for error reporting.
    Error = BuildGraph(fgmVidCap, CameraName & FILTERLIST, CONNECTIONLIST) + 1
    If Error >= 1 Then
        Set fgmVidCap = Nothing
        Exit Function
    End If
    
    Set bv2VidCap = fgmVidCap
    With bv2VidCap
        AspectRatio = CDbl(.VideoHeight) / CDbl(.VideoWidth)
    End With
    shpBorder.Visible = False
    Height = Width * AspectRatio
    
    Set vwVidCap = fgmVidCap
    With vwVidCap
        .FullScreenMode = False
        .Left = 0
        .Top = 0
        .Width = ScaleX(ScaleWidth, ScaleMode, vbPixels)
        .Height = ScaleY(ScaleWidth * AspectRatio, ScaleMode, vbPixels)
        If mBorderStyle = bsFixedSingle Then
            .WindowStyle = .WindowStyle And MASKBORDERMIN
        Else
            .WindowStyle = .WindowStyle And MASKBORDERLESS
        End If
        .Owner = hWnd
        .Visible = True
    End With
    
    StartCamera = True
    fgmVidCap.Run
End Function

Public Sub StopCamera()
    Const StopWaitMs As Long = 40
    Dim State As FILTER_STATE
    
    If Not fgmVidCap Is Nothing Then
        With fgmVidCap
            .Stop
            Do
                .GetState StopWaitMs, State
                Error = Err.Number
            Loop Until State = State_Stopped Or Error = E_FAIL
        End With
        Error = 0
        
        With vwVidCap
            .Visible = False
            .Owner = 0
        End With
        Set vwVidCap = Nothing
        Set bv2VidCap = Nothing
        Set fgmVidCap = Nothing
        
        Height = DesignHeight
        shpBorder.Visible = mBorderStyle = bsFixedSingle
    End If
End Sub

'=== Private Methods =================================================

Private Function BuildGraph( _
    ByVal FGM As QuartzTypeLib.FilgraphManager, _
    ByVal Filters As String, _
    ByVal Connections As String) As Integer
    'Returns -1 on success, or FilterIndex when not found, or
    'ConnIndex + 100 when a pin of the connection not found.
    '
    'Filters:
    '
    '   A string with Filter Name values separated by "|" delimiters
    '   and optionally each of these can be followed by one required
    '   Pin Name value separated by a "~" delimiter for use as a tie
    '   breaker when there might be multiple filters with the same
    '   Name value.
    '
    'Connections:
    '
    '   A string with a list of output pins to be connected to
    '   input pins.  Each pin-pair is separated by "|" delimiters
    '   and each pair has out and in pins separated by a "~"
    '   delimiter.  The pin-pairs should be one less than the number
    '   of filters.
    Dim FilterNames() As String
    Dim FilterIndex As Integer
    Dim FilterParts() As String
    Dim FoundFilter As Boolean
    Dim rfiEach As QuartzTypeLib.IRegFilterInfo
    Dim fiFilters() As QuartzTypeLib.IFilterInfo
    Dim Conns() As String
    Dim ConnIndex As Integer
    Dim ConnParts() As String
    Dim piEach As QuartzTypeLib.IPinInfo
    Dim piOut As QuartzTypeLib.IPinInfo
    Dim piIn As QuartzTypeLib.IPinInfo
    
    'Setup for filter script processing.
    FilterNames = Split(UCase$(Filters), "|")
    ReDim fiFilters(UBound(FilterNames))
    
    'Find and add filters.
    For FilterIndex = 0 To UBound(FilterNames)
        FilterParts = Split(FilterNames(FilterIndex), "~")
        For Each rfiEach In FGM.RegFilterCollection
            If UCase$(rfiEach.Name) = FilterParts(0) Then
                rfiEach.Filter fiFilters(FilterIndex)
                If UBound(FilterParts) > 0 Then
                    For Each piEach In fiFilters(FilterIndex).Pins
                        If UCase$(piEach.Name) = FilterParts(1) Then
                            FoundFilter = True
                            Exit For
                        End If
                    Next
                Else
                    FoundFilter = True
                    Exit For
                End If
            End If
        Next
        If FoundFilter Then
            FoundFilter = False
        Else
            BuildGraph = FilterIndex
            Exit Function 'Error result will be 0, 1, etc.
        End If
    Next
    BuildGraph = -1
    
    'Setup for connection script processing.
    Conns = Split(UCase$(Connections), "|")
    FilterIndex = 0
    
    'Find and connect pins.
    For ConnIndex = 0 To UBound(Conns)
        ConnParts = Split(Conns(ConnIndex), "~")
        For Each piEach In fiFilters(FilterIndex).Pins
            If UCase$(piEach.Name) = ConnParts(0) Then
                Set piOut = piEach
                Exit For
            End If
        Next
        For Each piEach In fiFilters(FilterIndex + 1).Pins
            If UCase$(piEach.Name) = ConnParts(1) Then
                Set piIn = piEach
                Exit For
            End If
        Next
        If piOut Is Nothing Or piIn Is Nothing Then
            'Error, missing a pin.
            BuildGraph = ConnIndex + 100 'Error result will be 100, 101, etc.
            Exit Function
        End If
        piOut.ConnectDirect piIn
        FilterIndex = FilterIndex + 1
    Next
End Function

'=== Event Handlers ==================================================

Private Sub UserControl_Initialize()
End Sub

Private Sub UserControl_InitProperties()
    BorderStyle = bsFixedSingle
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
        UserControl.BackColor = .ReadProperty("BackColor", vbButtonFace)
        BorderStyle = .ReadProperty("BorderStyle", bsFixedSingle)
    End With
    DesignHeight = Height
End Sub

Private Sub UserControl_Resize()
    shpBorder.Move 0, 0, ScaleWidth, ScaleHeight
End Sub

Private Sub UserControl_Terminate()
    StopCamera
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
        .WriteProperty "BackColor", UserControl.BackColor, vbButtonFace
        .WriteProperty "BorderStyle", mBorderStyle, bsFixedSingle
    End With
End Sub
