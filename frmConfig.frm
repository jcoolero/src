VERSION 5.00
Begin VB.Form frmConfig 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Configuration"
   ClientHeight    =   2235
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   4785
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2235
   ScaleWidth      =   4785
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtPicPath 
      Height          =   345
      Left            =   1530
      TabIndex        =   3
      Top             =   1230
      Width           =   3165
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Height          =   495
      Left            =   3540
      TabIndex        =   4
      Top             =   1680
      Width           =   1185
   End
   Begin VB.TextBox txtOfficerNumber 
      Height          =   345
      Left            =   1530
      TabIndex        =   2
      Top             =   840
      Width           =   3165
   End
   Begin VB.TextBox txtPresidentName 
      Height          =   345
      Left            =   1530
      TabIndex        =   0
      Top             =   60
      Width           =   3165
   End
   Begin VB.TextBox txtOfficerName 
      Height          =   345
      Left            =   1530
      TabIndex        =   1
      Top             =   450
      Width           =   3165
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pic Path"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   60
      TabIndex        =   8
      Top             =   1230
      Width           =   1455
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Office Number"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   60
      TabIndex        =   7
      Top             =   840
      Width           =   1455
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "President Name"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   60
      TabIndex        =   6
      Top             =   60
      Width           =   1455
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Officer Name"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   60
      TabIndex        =   5
      Top             =   450
      Width           =   1455
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rs As ADODB.Recordset


Private Sub cmdSave_Click()
If dao.updateConfig(txtPresidentName, txtOfficerName, txtOfficerNumber, txtPicPath) Then
    MsgBox "Config successfully saved."
End If
End Sub

Private Sub Form_Load()
Set rs = dao.getConfig
txtPresidentName = rs.Fields("presidentName")
txtOfficerName = rs.Fields("OfficerName")
txtOfficerNumber = rs.Fields("OfficerNumber")
txtPicPath = rs.Fields("picPath")
End Sub


Private Sub txtPresidentName_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, txtOfficerName
End Sub
Private Sub txtOfficerName_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, txtOfficerNumber
End Sub

Private Sub txtOfficerNumber_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, txtPicPath
End Sub

Private Sub txtPicPath_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, cmdSave
End Sub

