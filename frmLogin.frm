VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Login"
   ClientHeight    =   1560
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   3450
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1560
   ScaleWidth      =   3450
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdExit 
      Caption         =   "&Exit"
      Height          =   495
      Left            =   1020
      TabIndex        =   5
      Top             =   990
      Width           =   1095
   End
   Begin VB.CommandButton cmdLogin 
      Caption         =   "&Login"
      Height          =   495
      Left            =   2220
      TabIndex        =   2
      Top             =   990
      Width           =   1095
   End
   Begin VB.TextBox txtPassword 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1050
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   540
      Width           =   2265
   End
   Begin VB.TextBox txtUsername 
      Height          =   315
      Left            =   1050
      TabIndex        =   0
      Top             =   120
      Width           =   2265
   End
   Begin VB.Label lblPassword 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Password"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   60
      TabIndex        =   4
      Top             =   540
      Width           =   945
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Username"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   60
      TabIndex        =   3
      Top             =   150
      Width           =   945
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdExit_Click()
End
End Sub

Private Sub cmdLogin_Click()
If dao.validUser(txtUsername, txtPassword) Then
    username = txtUsername
Else
    MsgBox "Invalid User"
    username = ""
End If
Unload Me
End Sub

Private Sub txtUsername_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, txtPassword
End Sub
Private Sub txtPassword_KeyPress(KeyAscii As Integer)
enterKey KeyAscii, cmdLogin
End Sub

